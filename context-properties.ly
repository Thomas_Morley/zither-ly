\version "2.23.9"

%% Borrowed non-exported procedure in define-context-properties.scm
#(define (translator-property-description symbol type? description)
  (if (not (and
            (symbol? symbol)
            (procedure? type?)
            (string? description)))
      (throw 'init-format-error))
  (if (not (equal? #f (object-property symbol 'translation-doc)))
      (ly:warning (G_ "symbol ~S redefined") symbol))
  (set-object-property! symbol 'translation-type? type?)
  (set-object-property! symbol 'translation-doc description)
  (set! all-translation-properties (cons symbol all-translation-properties))
  symbol)

#(translator-property-description
  'chordDigits
  boolean?
  "If true print digits for zither chords, otherwise use characters.")

#(translator-property-description
  'printStringNames
  boolean?
  "Should string names be printed?.")

#(translator-property-description
  'emphazisedPitch
  ly:pitch?
  "Which pitch to print a thicker line for in @code{StaffSymbol} of
@code{ZitherStaff}.")

#(translator-property-description
  'emphaziseString
  boolean?
  "Print a thicker line in @code{StaffSymbol} of @code{ZitherStaff} for the
string with the pitch derived from context-property @code{emphazisedPitch}.")
