\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-dis''-tuning
  }
}

\header {
  zither-title = "Chords in ZitherStaff"
  zither-snippet-doc =
  \markup \column {
    "In \"Unterlegnoten\" for Akkordzither only"
    "certain chords are possible:"
    \pad-around #1
      \score {
        <<
          \new ChordNames \chordmode { c4 g f d a e }
          \new Staff {
            \clef "bass"
            \cadenzaOn
            <c e' c' g>4
            <g, d' b g>
            <f, f' c' a>
            <d fis a d'>
            <a, e' cis' a>
            <e, e' b gis>
          }
        >>
        \layout {
          indent = 10
          ragged-right = ##t
          \context {
            \ChordNames
            chordNameFunction = #ignatzek-chord-names
            \override ChordName.rotation = #'(-90 0 0)
            %\override ChordName.extra-offset = #'(2 . 0)
            \override ChordName.X-offset = 0
          }
        }
      }
    "These chord are counted: 1 is C-major, 2 is G-major, etc"
    "Chords may happen as bass only, without bass or as entire chord."
    "One possibilty is to print them using digits/dots/bows, p.e.:"
    \pad-around #1
      \score {
        \new Staff \chordmode {
          \clef "bass"
          \cadenzaOn
          \override TextScript.staff-padding = 4
          \override TextScript.font-size = #-2
          \override TextScript.font-encoding = #'fetaText
          c,2:1_\zitherFormatChordSimple "1"
          c/g_\zitherFormatChordUnderdot "1"
          c/g/+c_\zitherFormatChordUndertie "1"
        }
        \layout {
          indent = 10
          ragged-right = ##t
        }
      }
    "If the context-property `chordDigits' is set false (default is true),"
    "Characters are printed instead of digits."
    "Below those chords in all versions."
  }
}

basicChordsMel = {
  \time 3/4
  c''4 4 4
  g'4 4 4
  f'4 4 4
  d'4 4 4
  a'4 4 4
  e'4 4 4
}

%% TODO entering chords with chord-syntax like g,,:1.8.10.12 is tedious.
%%      Find better method
basicChordsChords = \chordmode {
  c4:1 c/g c/+c
  g:1 g g/+g
  f:1 f/a f/+f
  \set chordDigits = ##f
  d:1 d/fis d/+d
  a:1 a a/+a
  e:1 e/gis e/e
}

basicChordsMusic =
  <<
    \new ChordNames \basicChordsChords
    \basicChordsMel
  >>

\markup {
  \akkordZitherTab
  \basicChordsMusic
}
