\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-dis''-tuning
  }
}

\header {
  zither-title = "Repeats in ZitherStaff"
  zither-snippet-doc =
  \markup \column {
    "In \"Unterlegnoten\" for Akkordzither repeats are"
    "supported."
    "If \\repeat volta 2 { <body> } \\alternative { { <alt1> } { <alt2> } }"
    "is used, the line connecting the elements of the music stream"
    "is stopped at last element of <alt1> and"
    "restarted at first element of <alt2>."
    "This functionality is provided by the internal"
    "music-function `allGlissando'."
    "Additionaly a line is printed from last element of <body>"
    "to first element of <alt2>, done by the internal"
    "music-function `autoVoltaFollower'."
    "This line is annotated by some text, customizable by an override"
    "for TextSpanner.details.added-text."
    "Default is \\markup \\italic \\fontsize #-2 \"2ème x\""
    "This text may be moved by an override for"
    "TextSpanner.details.translate-text, taking a number-pair,"
    "default is '(1 . 1)"
    "TODO a straight line frequently causes collisions."
    "Implement a posibility to print is bow or the like."
  }
}

repeatsMel = \relative {
  \bar ".|:"
  \repeat volta 2 { f'8 g a b }
  %% We abuse StrokeFinger to print a fake BarLine, thus we need to override
  %% StrokeFinger not BarLine to get more space
  \once \override ZitherStaff.StrokeFinger.extra-spacing-width = #'(-1.5 . 0)
  \repeat volta 2 {
    b a g f
  }
  \alternative {
    { c'2 c }
    {
      f, f
      \once
        \override ZitherStaff.StrokeFinger.extra-spacing-width = #'(-1.5 . 0)
    }
  }
  \override TextSpanner.details.translate-text = #'(-1 . -1.3)
  \override TextSpanner.details.added-text = \markup \fontsize #-2 "2nd time"
  \repeat volta 2 {
    f8 g a b
    \override ZitherStaff.StrokeFinger.X-extent = #'(-4 . 4)
  }
  \alternative {
    {
      c4
      c
      %% TODO more space for the fake BarLine is needed, why does it not work
      %%      as above?
    }
    { c a }
  }
  f1
}


\markup {
  \akkordZitherTab
  \repeatsMel
}
