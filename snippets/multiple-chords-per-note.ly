\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-dis''-tuning
  }
}

\header {
  zither-title = "Multiple chords per note"
  zither-snippet-doc =
  \markup \column {
    "In \"Unterlegnoten\" for Akkordzither it is common"
    "to print sequential chords during a long note not"
    "distributed along the connecting line, but right"
    "after the initial chord of said long note."
    "Such stacked rows of chords collide more often"
    "with other stuff."
    "You may want to apply \\moveChord taking a number-pair"
    "for offsetting in X/Y axis direction."
  }
}

multipleChordsMel = {
  \time 4/4
  d'1
  e'2.. e'8
  f'1
}

%% TODO entering chords with chord-syntax like g,,:1.8.10.12 is tedious.
%%      Find better method, for now we use common event-chords
multipleChords = \chordmode {
  \moveChord #'(-0.6 . -0.4) <d>4 <fis a d'> <d fis a d'> <d>
  <e,>4 <e' b gis> <e, e' b gis> <e,>
  <f,>2 q
}

multipleChordsMusic =
  <<
    \new ChordNames \multipleChords
    \multipleChordsMel
  >>

\markup {
  \akkordZitherTab
  \multipleChordsMusic
}
