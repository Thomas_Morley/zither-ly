\version "2.23.9"

%% NOTA BENE
%%
%% Currently we use "a4landscape", which should be rotated to `portrait' later
%%     With landscape it looks like (graphic annotated with defaults):
%%
%%                    top-margin
%%                 ----------------
%%                 |              |
%%     left-margin |              | right-margin
%%                 |              |
%%                 ----------------
%%                   bottom-margin
%%
%%     After rotation (portrait) it becomes:
%%
%%                   left-margin
%%                    ---------
%%                    |       |
%%                    |       |
%%     bottom-margin  |       | top-margin
%%                    |       |
%%                    |       |
%%                    |       |
%%                    ---------
%%                   right-margin
%%

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

\paper {
  %% defines the printable area
  %% top/bottom/left/right refers to landscape
  top-margin = 5
  bottom-margin = 5
  left-margin = 5
  right-margin = 5
  %% set spacing within the printable area
  %% top/bottom/left/right refers to portrait
  %%
  %% TODO this inconsistent, try to let margins refer to portrait as well
  zither-top-markup-spacing.padding = 10
  zither-last-bottom-spacing.padding = 10
  zither-markup-system-spacing.padding = 10
  zitherBookTitleMarkup =
    \markup \box \akkordZitherBookTitle
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-dis''-tuning
    printStringNames = ##t
  }
}


\header {
  zither-title = "Spacing in ZitherStaff"
  zither-snippet-doc = \markup
    \column {
    \string-lines
      #"For better viewing several printed elements are boxed.
      To define the printable area use margins, though note the
      whole thing is rotated, see commented graphic above.
      In this example all margins are set to 5, default 4.
      To control spacing even more use special `zither-top-markup-spacing',
      `zither-last-bottom-spacing' and `zither-markup-system-spacing' like
      their default variants.
      Note,
      (1) zither-xxx-spacing is relative to the already rotated page
      (2) only setting `padding' will take any effect."
    }
}

spacingMel = {
  g'1
}

\markup \box {
  \akkordZitherTab
  \spacingMel
}