\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-dis''-tuning
  }
}

\header {
  zither-title = "Ties in ZitherStaff"
  zither-snippet-doc =
  \markup \column {
    "In \"Unterlegnoten\" for Akkordzither Ties are never printed."
    "Tied TabNoteHeads are always parenthesized."
    "Tied TabNoteHeads can take chords"
  }
}

tieMel = {
  d'1~ 1
  fis'2.~ 4
  a'4~ 4
  d''8~ 8
  d'8~ 8
}

tieChords = \chordmode {
  d,1:1 d,1:1.3.5.8
  d,2.:1 d,4:1.3.5.8
  a,,4:1 a,,:1.8.10.12
  d,8:1 d,:1.3.5.8
  d,8:1 d,:1.3.5.8
}

tieMusic =
  <<
    \new ChordNames \tieChords
    \tieMel
  >>

\markup {
  \akkordZitherTab
  \tieMusic
}
