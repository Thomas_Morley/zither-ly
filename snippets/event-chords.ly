\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-dis''-tuning
  }
}

\header {
  zither-title = "Event Chords"
  zither-snippet-doc =
  \markup \column {
    "In \"Unterlegnoten\" for Akkordzither event chords"
    "entered with <...>-syntax are supported."
    "The line connecting sequential notes and event-chords is printed"
    "only for the first entered note of an event-chord."
    "Notes of an event chord belonging not to the melody,"
    "typically the ones below the top one, will get no Stem."
    "A horizontal line is printed to connect all notes of an event-chord."
    "If repeat bar lines are present, they are printed at"
    "the note with the highest pitch."
  }
}

eventChordsMel = {
  <f' e' dis'>4 <g' e'> <a' f'> <b' g'> | %% 1
  \repeat volta 2 { f'4 <g' e'> a' <b' g'> } |  %% 2
  \repeat volta 2 { <cis'' gis' >2 } <f' d'>2  | %% 3

  %% EvtChrd in repeat body
  \repeat volta 2 { <g' e'>4 } \alternative { { ees'4 } { b'4 } }

  %% EvtChrd in first volta

  \repeat volta 2 { g'4 } \alternative { { <ais' gis'>4 } { bes'4 } } | %% 4

  %% EvtChrd in second volta

  \repeat volta 2 { e'4 g' f' } \alternative { { e'4 } { <ais' gis'>4 } } | %% 5
  c'1 | %% 6
  \bar "|."
}

eventChords = \chordmode {
  d4:1 e4:1 f4:1 g4:1 | %% 1
  \repeat volta 2 { d4:1 e4:1 f4:1 g4:1 } | %% 2
  \repeat volta 2 { c4:1 c/e c2/+c } | %% 3
  \repeat volta 2 { d4:1 } \alternative { { c4 } { c } }
  \repeat volta 2 { d4:1 } \alternative { { c4 } { c } } | %% 4
  \repeat volta 2 { d2.:1 } \alternative { { c4 } { c } } | %% 5
  c4/+c f/c c2/+c | %% 6
}

eventChordsMusic =
  <<
    \new ChordNames \eventChords
    \eventChordsMel
  >>

\markup {
  \akkordZitherTab
  \eventChordsMusic
}
