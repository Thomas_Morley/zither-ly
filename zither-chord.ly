\version "2.23.9"

\include "context-properties.ly"

#(define-markup-command (underdot layout props arg)(markup?)
  #:properties ((offset 0.4))
  "Put a single dot centered below @var{arg}"
  (let* ((font
           (ly:paper-get-font
             layout
             (cons
               '((font-encoding . fetaMusic) (font-name . #f))
               props)))
         (dot (ly:font-get-glyph font "dots.dot"))
         (dot-x-ext (ly:stencil-extent dot X))
         (arg-stil (interpret-markup layout props arg))
         (arg-x-ext (ly:stencil-extent arg-stil X))
         (arg-y-ext (ly:stencil-extent arg-stil Y)))
    (ly:stencil-add
      arg-stil
      (ly:stencil-translate
        dot
        (cons
          (- (interval-center arg-x-ext)
             (interval-center dot-x-ext))
          (- (car arg-y-ext) offset))))))

zitherFormatChordSimple =
#(define-scheme-function (arg)(markup?)
"Returns a makup with @var{arg} formated with'font-encoding set to 'fetaText"
#{
 \markup
   $arg
#})

zitherFormatChordUnderdot =
#(define-scheme-function (arg)(markup?)
"Returns a makup with @var{arg} formated with'font-encoding set to 'fetaText and
@code{underdot-markup}."
#{
 \markup
   \underdot $arg
#})

zitherFormatChordUndertie =
#(define-scheme-function (arg)(markup?)
"Returns a makup with @var{arg} formated with 'font-encoding set to 'fetaText,
@code{undertie-markup}, 'thickness set to 2.0 and 'height-limit set to 1.0 "
#{
 \markup
   \override #'(thickness . 2.0)
   \override #'(height-limit . 1.0)
   \undertie $arg
#})

#(define zither::chord-names
  (lambda (in-pitches bass inversion context)
   (let* ((default-name
            (ignatzek-chord-names in-pitches bass inversion context))
          (root (car in-pitches))
          ;; NB pitch-notename is an integer
          (root-pitch-notename (ly:pitch-notename root))
          (chord-digits? (ly:context-property context 'chordDigits #t))
          (chord-list
            ;; TODO make these lists a context-property?
            (if chord-digits?
                '("1" "4" "6" "3" "2" "5")
                '("C" "D" "E" "F" "G" "A")))
          (root-string
            (list-ref chord-list root-pitch-notename))
          (root-markup
            (if chord-digits?
                #{
                   \markup
                     \override #'(font-encoding . fetaText)
                     $root-string
                #}
                root-string)))
     (cond ((= 1 (length in-pitches)) (zitherFormatChordSimple root-markup))
           ((ly:pitch? bass) (zitherFormatChordUndertie root-markup))
           (else (zitherFormatChordUnderdot root-markup))))))

moveChord =
#(define-music-function (x-y-amount)(pair?)
"Applies 'extra-offset to ChordName-grobs."
;; We want to do this only in TabStaff, thus using applyOutput
#{
  \applyOutput ZitherStaff.ChordName
    #(lambda (grob ctx c)
    (ly:grob-set-property!
      grob
      'extra-offset
      (cons (- (cdr x-y-amount)) (- (car x-y-amount)))))
#})
