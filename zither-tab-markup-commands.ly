\version "2.23.9"

#(define-markup-command (bar-line layout props strg)(string?)
  #:properties ((font-size 0)
                (add-height 0))
"Returns bar-lines as markup"

  (define (string->string-list str)
;    "Convert a string into a list of strings with length 1.
;  @code{\"aBc\"} will be converted to @code{(\"a\" \"B\" \"c\")}.
;  An empty string will be converted to a list containing @code{""}."
    (if (and (string? str)
             (not (zero? (string-length str))))
        (map (lambda (s)
                     (string s))
             (string->list str))
        (list "")))

  (define (make-bar-line thickness height fontsize blot-diameter)
    "Draw a simple bar line."
    (ly:round-filled-box
      (cons 0 (* (magstep font-size) thickness))
      (interval-widen (cons 0 (+ add-height height)) (magstep font-size))
      blot-diameter))

  (define (make-colon-bar-line height fontsize)
    (let* ((font
             (ly:paper-get-font layout
               (cons '((font-encoding . fetaMusic)) props)))
           (dot (ly:font-get-glyph font "dots.dot")))
    (ly:stencil-add
        dot
        (ly:stencil-translate-axis
          dot
          (magstep (+ (/ add-height 2) font-size))
          Y))))

  (define (make-bracket-bar-line height fontsize dir thick-bar )
    "Draw a bracket-style bar line. If @var{dir} is set to @code{LEFT}, the
     opening bracket will be drawn, for @code{RIGHT} we get the
     closing bracket."
    (let* ((font
              (ly:paper-get-font layout
                (cons '((font-encoding . fetaMusic)) props)))
           (brackettips-up (ly:font-get-glyph font "brackettips.up"))
           (brackettips-down (ly:font-get-glyph font "brackettips.down"))
           ;; the x-extent of the brackettips must not be taken into account
           ;; for bar line constructs like "[|:", so we set new bounds:
           (tip-up-stil
             (ly:make-stencil
               (ly:stencil-expr brackettips-up)
               (cons 0 0)
               (ly:stencil-extent brackettips-up Y)))
           (tip-down-stil
             (ly:make-stencil
               (ly:stencil-expr brackettips-down)
               (cons 0 0)
               (ly:stencil-extent brackettips-down Y)))
           (stencil
             (ly:stencil-add
               thick-bar
               (ly:stencil-translate-axis
                 tip-up-stil
                 (+ (/ (+ add-height height) 2) (magstep fontsize)  )
                 Y)
               (ly:stencil-translate-axis
                 tip-down-stil
                 (* -1 (+ (/ (+ add-height height) 2) (magstep fontsize) ))
                 Y))))
        (if (eq? dir LEFT)
            stencil
            (ly:stencil-scale stencil -1 1))))

;  (define (make-brace-bar-line height fontsize)
;    (let* ((new-height (max (* add-height height) 12))
;           (font
;             (ly:paper-get-font layout
;               (cons '((font-encoding . fetaBraces)
;                       (font-name . #f))
;                     props)))
;           (glyph-count (1- (ly:otf-glyph-count font)))
;           (scale (ly:output-def-lookup layout 'output-scale))
;           (scaled-size (+ add-height (magstep fontsize) (/ (ly:pt new-height) scale)))
;           (glyph
;             (lambda (n)
;               (ly:font-get-glyph
;                 font
;                 (string-append "brace" (number->string n)))))
;           (get-y-from-brace
;             (lambda (brace)
;               (interval-length
;                (ly:stencil-extent (glyph brace) Y))))
;           (find-brace
;             (binary-search 0 glyph-count get-y-from-brace scaled-size))
;           (glyph-found (glyph find-brace)))
;
;    (if (or (null? (ly:stencil-expr glyph-found))
;            (< scaled-size
;               (interval-length (ly:stencil-extent (glyph 0) Y)))
;            (> scaled-size
;               (interval-length (ly:stencil-extent (glyph glyph-count) Y))))
;        (begin
;          (ly:warning (_ "no brace found for point size ~S ") new-height)
;          (ly:warning (_ "defaulting to ~S pt")
;          (/ (* scale (interval-length (ly:stencil-extent glyph-found Y)))
;             (ly:pt 10)))))
;    glyph-found))
;;;;;

    (let* ((line-thickness (ly:output-def-lookup layout 'line-thickness))
           (size (ly:output-def-lookup layout 'text-font-size 12))
           ;; Numerical values taken from the IR
           (thin-thickness (* line-thickness 1.9))
           (kern (* line-thickness 3.0))
           (thick-thickness (* line-thickness 6.0))
           (blot (ly:output-def-lookup layout 'blot-diameter))
           ;; Not sure about the numerical value, found it by try and error.
           (thin-bar-line
             (ly:stencil-aligned-to
               (make-bar-line thin-thickness (/ size 9) font-size blot)
               Y CENTER))
           (thick-bar-line
             (ly:stencil-aligned-to
               (make-bar-line thick-thickness (/ size 9) font-size blot)
               Y CENTER))
           (colon-bar-line
             (ly:stencil-aligned-to
               (make-colon-bar-line (/ size 9) font-size)
               Y CENTER))
           (bracket-left-bar-line
             (ly:stencil-aligned-to
               (make-bracket-bar-line (/ size 9) font-size -1 thick-bar-line)
               Y CENTER))
           (bracket-right-bar-line
             (ly:stencil-aligned-to
               (make-bracket-bar-line (/ size 9) font-size 1 thick-bar-line)
               Y CENTER))
           (strg-list (string->string-list strg))
           (find-bar-line-proc
              (lambda (x) (cond ((string=? ":" x) colon-bar-line)
                                ((string=? "|" x) thin-bar-line)
                                ((string=? "." x) thick-bar-line)
                                ((string=? "[" x) bracket-left-bar-line)
                                ((string=? "]" x) bracket-right-bar-line)
                                (else empty-stencil))))
           (bar-line-stils
             (remove ly:stencil-empty? (map find-bar-line-proc strg-list)))
           (stil
             (stack-stencils X
                             RIGHT
                             (* (magstep font-size) kern)
                             bar-line-stils))
           (stil-y-length (interval-length (ly:stencil-extent stil Y))))
      ;; Hm, should the stencil-translate be hard-coded?
      (ly:stencil-translate-axis
        stil
        (/ stil-y-length 4)
        Y)))

%% LSR 467
#(define-markup-command (with-header layout props markup) (markup?)
  "Interpret the given markup with the header fields added to the props.
This way, one can re-use the same functions (using fromproperty
#'header:field) in the header block and as top-level markup."
;; TODO: If we are inside a score, add the score's local header block, too!
;; Currently, I only use the global header block, stored in $defaultheader
  (let ((scopes (list $defaultheader)))
    (if (every module? scopes)
        (let* ((alists (map ly:module->alist scopes))
               (prefixed-alist
                (map
                  (lambda (alist)
                     (map
                       (lambda (entry)
                         (cons
                           (symbol-append 'header: (car entry))
                           (cdr entry)))
                       alist))
                  alists))
               (props
                 (append
                   prefixed-alist
                   props
                   (layout-extract-page-properties layout))))
          (interpret-markup layout props markup))
        (begin
          (ly:warning "No header-module found, returning point-stencil")
          point-stencil))))

#(define-public (current-string-tuning output-def context-name)
"Look up and return current string tuning in @var{output-def} of
@var{context-name} typically @code{$defaultlayout}"
  (let* ((default-layout
           (ly:module->alist (ly:output-def-scope output-def)))
         (context-def
           (assoc-get context-name default-layout))
         (property-ops
           (ly:context-def-lookup context-def 'property-ops))
         ;; There may be more than one stringTunings, get the one on top of
         ;; the stack.
         ;; They appear like (assign stringTunings <pitch-list>)
         (string-tuning-list
           (filter
             (lambda (ops)
               (equal? (take ops 2) '(assign stringTunings)))
             property-ops))
         (current-strings
           (reverse (third (car string-tuning-list)))))
     current-strings))

#(define-markup-command (boxed-pitch-names-row layout props)()
  #:properties ((context-name 'ZitherStaff))
;; TODO make it even more generic, i.e. change above to TabStaff and let
;;      boxes better handle smaller staff-space
"Return a row of boxes with german pitchnames in it.  Altered pitches are
on grey backfround.  Otherwise the background is white."
  (let* ((tuning-pitches
           (current-string-tuning $defaultlayout context-name))
         (tuning-pitch-names
           (map
             (lambda (ev) ((chord-name->german-markup #t) ev #f))
             tuning-pitches))
         (strngs (length tuning-pitch-names))
         (default-layout (ly:output-def-scope $defaultlayout))
         (context-def
           (assoc-get context-name (ly:module->alist default-layout)))
         (property-ops
           (ly:context-def-lookup context-def 'property-ops))
         (staff-space
           (car
             (filter-map
               (lambda (op)
                 (and (equal? (last op) 'staff-space)
                      (third op)))
               property-ops)))
         (boxes
           (map-in-order
             (lambda (alt x)
               (make-combine-markup
                 (make-filled-box-markup
                   (cons (/ staff-space -4)  staff-space)
                   (cons (/ staff-space -2) (/ staff-space 2))
                   0)
                 (make-with-color-markup
                   (if (zero? alt) white '(0.8 0.8 0.8))
                   (make-filled-box-markup
                     (cons
                       (+ (/ staff-space -4) 0.1)
                       (- staff-space 0.1))
                     (cons
                       (+ (/ staff-space -2) 0.1)
                       (- (/ staff-space 2) 0.1))
                     0))))
               (map ly:pitch-alteration (reverse tuning-pitches))
               (iota strngs 1 1)))
         (pitch-mrkps
           #{
              \markuplist
                \rotate #90
                \vcenter
                \hcenter-in #staff-space
                $(reverse tuning-pitch-names)
           #})
         (instrument-name-markup
           #{
              \markup \overlay { \column $boxes \right-column $pitch-mrkps }
           #}))
  (interpret-markup layout props instrument-name-markup)))

#(define-markup-command (zither-header-tab layout props music)(ly:music?)
    #:category music
    #:properties ((tab-offset 5))
    "DOCME"
    (let* ((defaultpaper-alist
             (ly:module->alist (ly:output-def-scope $defaultpaper)))
           (papersizename
             (if (ly:output-def? $defaultpaper)
                 (assoc-get
                  'papersizename
                  defaultpaper-alist)
                 ""))
           (landscape?
             (string-contains papersizename "landscape"))
           (paper-width
             (ly:output-def-lookup
               layout
               (if landscape?
                   'paper-width
                   'paper-height)))
           (paper-height
             (ly:output-def-lookup
               layout
               (if landscape?
                   'paper-height
                   'paper-width)))
           (left-margin (ly:output-def-lookup layout 'left-margin))
           (right-margin (ly:output-def-lookup layout 'right-margin))
           (top-margin (ly:output-def-lookup layout 'top-margin))
           (bottom-margin (ly:output-def-lookup layout 'bottom-margin))
           (output-scale (ly:output-def-lookup layout 'output-scale))
           (zither-book-title-markup
             (ly:output-def-lookup layout 'zitherBookTitleMarkup))
           (default-layout (ly:output-def-scope $defaultlayout))
           (zither-staff-context-def
             (assoc-get 'ZitherStaff (ly:module->alist default-layout)))
           (property-ops
             (ly:context-def-lookup zither-staff-context-def 'property-ops))
           (print-strings?
             (last
               (car
                 (filter-map
                   (lambda (ops)
                     (and (equal? (cadr ops) 'printStringNames)
                          ops))
                   property-ops))))
           (strings-stencil
             (if print-strings?
                 (interpret-markup layout props
                   #{
                      \markup
                        \rotate #(if landscape? 0 -90)
                        \boxed-pitch-names-row
                   #})
                   empty-stencil))
           (string-stencil-x-ext
             (ly:stencil-extent strings-stencil X))
           (string-stencil-y-ext
             (ly:stencil-extent strings-stencil Y))
           (string-stencil-width (interval-length string-stencil-x-ext))
           (string-stencil-height (interval-length string-stencil-y-ext))
           (markup-system-padding
             (assoc-get
               'padding
               (ly:output-def-lookup
                 layout
                 (if landscape?
                     'zither-markup-system-spacing
                     'markup-system-spacing))
                 0))
           (last-bottom-padding
             (assoc-get
               'padding
               (ly:output-def-lookup
                 layout
                 (if landscape?
                     'zither-last-bottom-spacing
                     'last-bottom-spacing))
               0))
           (top-markup-padding
             (assoc-get
               'padding
               (ly:output-def-lookup
                 layout
                 (if landscape?
                     'zither-top-markup-spacing
                     'top-markup-spacing))
               0))
           (header-markup
             #{
                \markup
                  \rotate #(if landscape? 90 0)
                  \override
                    #(cons 'line-width
                           (- paper-height
                              (if landscape?
                                  (+ top-margin bottom-margin)
                                  (+ left-margin right-margin))))
                  \fill-line {
                    %% 10 is my choice, harm
                    \pad-x #10
                    \override #(cons 'line-width
                                     (- paper-height
                                        top-markup-padding
                                        last-bottom-padding))
                    $zither-book-title-markup
                    \null
                  }
             #})

           (header-stencil
             (interpret-markup layout props header-markup))
           (header-x-ext (ly:stencil-extent header-stencil X))
           (header-width (interval-length header-x-ext))
           (header-y-ext (ly:stencil-extent header-stencil Y))
           (header-height (interval-length header-y-ext))
           (header-padding
             (if (zero? header-width)
                 0
                 markup-system-padding))
           (tab-line-width
             (- paper-width
                (if landscape? header-width header-height)
                (if landscape?
                    (+ right-margin left-margin)
                    (+ top-margin bottom-margin))
                top-markup-padding
                header-padding
                (if landscape? string-stencil-width string-stencil-height)
                last-bottom-padding))
           ;; Seems we do not need it here
           ;(current-zither-tuning
           ;  (reverse (current-string-tuning $defaultlayout)))
           (zither-tab-stencil
             (interpret-markup layout props
               #{
                 \markup
                   \rotate #(if landscape? 0 -90)
                   \score {
                     \new ZitherStaff {
                       \autoVoltaFollower
                       \allGlissando
                       $music
                     }
                     \layout {
                       line-width = #(* tab-line-width output-scale)
                     }
                   }
               #}))
           (translated-tab-strings-stencil
             (ly:stencil-translate
               (ly:stencil-combine-at-edge
                 (ly:stencil-translate
                   (ly:stencil-aligned-to zither-tab-stencil X CENTER)
                   (cons 0 0))
                 (if landscape? X Y)
                 (if landscape? RIGHT DOWN)
                 (ly:stencil-aligned-to
                   strings-stencil
                   (if landscape? Y X)
                   CENTER)
                 0.05)
               (cons
                 (/ string-stencil-width 2)
                 0)))
           (translated-header-stencil
             (ly:stencil-translate
               header-stencil
               (cons
                 (if landscape?
                     (- top-markup-padding (car header-x-ext))
                     0)
                 0))))
    (ly:stencil-combine-at-edge
      translated-header-stencil
      (if landscape? X Y)
      (if landscape? RIGHT DOWN)
      translated-tab-strings-stencil
      markup-system-padding)))
