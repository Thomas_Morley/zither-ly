\version "2.23.9"

zitherTuning =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g'' gis'' a'' ais'' b'' c'''
>

zitherTuningXXV =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g'' gis'' a'' ais'' b'' c'''
>

zitherTuningXXIV =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g'' gis'' a'' ais'' b''
>

zitherTuningXXIII =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g'' gis'' a'' ais''
>

zitherTuningXXII =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g'' gis'' a''
>

zitherTuningXXI =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g'' gis''
>

zitherTuningXX =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis'' g''
>

zitherTuningXIX =
<
 c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
 c'' cis'' d'' dis'' e'' f'' fis''
>

zitherTuningXVIII =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b' c'' cis'' d'' dis'' e'' f'' >

zitherTuningXVII =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b' c'' cis'' d'' dis'' e'' >

zitherTuningXVI =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b' c'' cis'' d'' dis'' >

zitherTuningXV =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b' c'' cis'' d''>

zitherTuningXIV =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b' c'' cis''>

zitherTuningXIII =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b' c''>

zitherTuningXII =
< c' cis' d' dis' e' f' fis' g' gis' a' ais' b'>

zitherTuningXI =
< c' cis' d' dis' e' f' fis' g' gis' a' ais'>

zitherTuningX =
< c' cis' d' dis' e' f' fis' g' gis' a'>

\makeDefaultStringTuning #'zither-c'-c'''-tuning \stringTuning \zitherTuningXXV
\makeDefaultStringTuning #'zither-c'-b''-tuning \stringTuning \zitherTuningXXIV
\makeDefaultStringTuning #'zither-c'-ais''-tuning \stringTuning \zitherTuningXXIII
\makeDefaultStringTuning #'zither-c'-a''-tuning \stringTuning \zitherTuningXXII
\makeDefaultStringTuning #'zither-c'-gis''-tuning \stringTuning \zitherTuningXXI
\makeDefaultStringTuning #'zither-c'-g''-tuning \stringTuning \zitherTuningXX
\makeDefaultStringTuning #'zither-c'-fis''-tuning \stringTuning \zitherTuningXIX
\makeDefaultStringTuning #'zither-c'-f''-tuning \stringTuning \zitherTuningXVIII
\makeDefaultStringTuning #'zither-c'-e''-tuning \stringTuning \zitherTuningXVII
\makeDefaultStringTuning #'zither-c'-dis''-tuning \stringTuning \zitherTuningXVI
\makeDefaultStringTuning #'zither-c'-d''-tuning \stringTuning \zitherTuningXV
\makeDefaultStringTuning #'zither-c'-cis''-tuning \stringTuning \zitherTuningXIV
\makeDefaultStringTuning #'zither-c'-c''-tuning \stringTuning \zitherTuningXIII
\makeDefaultStringTuning #'zither-c'-b'-tuning \stringTuning \zitherTuningXII
\makeDefaultStringTuning #'zither-c'-ais'-tuning \stringTuning \zitherTuningXI
\makeDefaultStringTuning #'zither-c'-a'-tuning \stringTuning \zitherTuningX

\layout {
  indent = 0
  ragged-right = ##f
  ragged-last-bottom = ##f
   \context {
     \Score
     \accepts "ZitherStaff"
     \remove "Volta_engraver"
     %\remove "Timing_translator"
     %\remove "Default_bar_line_engraver"
     chordNameFunction = #zither::chord-names
     slashChordSeparator = ""
     chordNoteNamer = #(lambda (bass lowercase?) "")
     noChordSymbol = ""
     chordDigits = ##t
     \hide SystemStartBar
     \override NonMusicalPaperColumn.page-break-permission = ##f
     \override NonMusicalPaperColumn.line-break-permission = ##f
     \omit BarNumber
     \override SpacingSpanner.uniform-stretching = ##t
   }
   \context {
     \TabStaff
     \name "ZitherStaff"
     \alias "TabStaff"
     \accepts "ZitherVoice"
     \accepts "ChordNames"
     \defaultchild "ZitherVoice"
     \consists \Merge_bar_line_glissando_engraver
     \consists \Fake_repeat_bars_engraver
     \consists \Stacked_zither_chords_engraver
     \consists "New_fingering_engraver"
     \consists "Volta_engraver"

     %% StaffSymbol
     \override StaffSymbol.staff-space = 5.0
     \override StaffSymbol.color = #'(0.8 0.8 0.8)
     stringTunings = #zither-c'-c'''-tuning
     printStringNames = ##t
     emphazisedPitch = #(ly:make-pitch 0 0) %% c' other syntax: ##{ c' #}
     emphaziseString = ##t
     %% `staffSymbolWithOneThickLine' prints a thick line for the string with
     %% the pitch taken from`emphazisedPitch'.
     %% This can be enabled/cancelled by setting `emphaziseString' true or false
     \staffSymbolWithOneThickLine

     %% Clef
     %% made transparent, not omitting, to keep the space
     \hide Clef
     \omit BarLine
     \omit VoltaBracket

     %% In Zither no Ties happen.
     %% Nevertheless, the tied TabNoteHead should always be parenthesized
     %% First revert the TabStaff's default, then apply the version for Zither.
     %% Do it before-line-breaking, because line-breaks never happen.
     \revert Tie.after-line-breaking
     \override Tie.before-line-breaking = #tie::handle-zither-tab-note-head
     %\revert Tie.stencil

     %% For event-chord Stems are useful
     \override Stem.stencil = #event-chord-connecting-line
     \override Stem.color = #(make-list 3 0.6)
     \override Stem.layer = #-200
   }
   \context {
     \TabVoice
     \name "ZitherVoice"
     \consists \Stem_notes_engraver

     %% NoteHeads
     \override TabNoteHead.stencil = #zither::tab-note-head
     \override TabNoteHead.whiteout = ##f

     %% TextSpanner
     %% used to indicate the sequence to follow in case of volta-repeats
     %% TODO there's something fishy with left/right padding, investigate!!
     \override TextSpanner.stencil = #ly:line-spanner::print
     \override TextSpanner.bound-details =
      #'((left  (padding . 0)
                (attach-dir . -1))
         (right (padding . 0)
                (attach-dir . -1)))
     \override TextSpanner.staff-padding = #'()
     \override TextSpanner.outside-staff-priority = #'()
     \override TextSpanner.dash-fraction = 0
     \override TextSpanner.dash-period = 0.2
     \override TextSpanner.color = #red
     \override TextSpanner.thickness = 3
     \override TextSpanner.Y-offset = #0
     \override TextSpanner.layer = -1000
     \override TextSpanner.details.added-text =
       \markup \italic \fontsize #-2 "2ème x"

     %% Glissando
     %% used to connect the notes

     %% only connect the first entered note of consecutive event-chords
     glissandoMap = #'((0 . 0))
     \override Glissando.bound-details.left.padding = 0.6
     \override Glissando.bound-details.right.padding = 0.6
     \override Glissando.bound-details.right.attach-dir = #1
     \override Glissando.extra-dy = 0
     \override Glissando.Y-offset = #0.1
     \override Glissando.after-line-breaking = ##t
     \override Glissando.breakable = ##t

     %% Stem
     %% TODO why '(1 . 0) ??
     \override TabNoteHead.stem-attachment = #'(1 . 0)

     \autoBeamOff
   }
   \context {
     \ChordNames
     \override ChordName.font-size = #-2
     \override ChordName.font-series = #'bold
%     \override ChordName.color = #red
     \setTabNoteHeadChordNameParent
     \override ChordName.Y-offset = 2
     \override ChordName.X-offset = #-2
     \override ChordName.stencil =
       #(grob-transformer 'stencil
         (lambda (grob orig) (ly:stencil-rotate orig 90 0 0)))

   }
}
