\version "2.23.9"

\paper {
   top-margin = 0
   bottom-margin = 0
   left-margin = 1 \cm
   right-margin = 0

   %% portrait
   top-markup-spacing = #'((padding . 2))
   markup-system-spacing = #'((padding . 2))
   last-bottom-spacing = #'((padding . 2))

   %% landscape
   zither-top-markup-spacing.padding = 2
   zither-markup-system-spacing.padding = 2
   zither-last-bottom-spacing.padding = 2

   bookTitleMarkup = ##f
   scoreTitleMarkup = ##f
}
