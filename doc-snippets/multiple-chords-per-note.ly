\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a6")

\paper {
  oddHeaderMarkup = ##f
  left-margin = 1 \cm
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-a'-tuning
  }
}

multipleChordsMel = {
  \time 4/4
  d'1
  e'2.. e'8
  f'1
}


multipleChords = \chordmode {
  <d>4 <fis a d'> <d fis a d'> <d>
  <e,>4 e/gis <e, e' b gis> <e,>
  <f,>2 q
}

multipleChordsMusic =
  <<
    \new ChordNames \multipleChords
    \multipleChordsMel
  >>

\markup {
  \akkordZitherTab
  \multipleChordsMusic
}
