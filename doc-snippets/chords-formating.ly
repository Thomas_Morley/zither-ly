\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a4")

\paper { oddHeaderMarkup = ##f }

basicChordsChords = \chordmode {
  \time 3/4
  \revert ChordName.stencil
  \revert ChordName.Y-offset
  \revert ChordName.X-offset
  c,4:1 c/g c/g/+c
  g,,:1 g, g,/+g
  f,,:1 f/a f/a/+f
  <>_"\set chordDigits = ##f is applied"
  \set chordDigits = ##f
  d,:1 d/fis d/fis/+d
  a,,:1 a, a,/+a
  e,,:1 e/gis e/gis/+e
  \bar "|."
}

<<
  \new ChordNames \basicChordsChords
  \new Staff { \clef bass \basicChordsChords }
>>
