\version "2.23.9"

\include "../zither-tab-markup-commands.ly"

\layout {
  \context {
    \TabStaff
    \override StaffSymbol.staff-space = 5
    stringTunings = \stringTuning
    <
      c' cis' d' dis' e' f' fis' g' gis' a' ais' b'
      c'' cis'' d'' dis'' e'' f'' fis'' g'' gis'' a''
    >
  }
}

\markup
  \halign #LEFT
  \rotate #-90
  \override #'(context-name . TabStaff)
  \boxed-pitch-names-row
