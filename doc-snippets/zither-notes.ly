\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a6")

\paper {
  oddHeaderMarkup = ##f
  left-margin = 1 \cm
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-ais'-tuning
    \override Glissando.color = #red
    \override Glissando.before-line-breaking =
      #(lambda (grob) (ly:grob-set-property! grob 'stencil #f))
  }
}

notesMel = \new ZitherVoice \relative {
  d'1 e2 f4 g8. a16 |
  a2.~ a4
  <a f d>4..
  q16
  q2
  %% TupletBracket is not yet supported
  % \override ZitherStaff.TupletBracket.Y-offset = #20
  % \revert ZitherStaff.TupletBracket.stencil
  % \tuplet 3/2 { d,4 d d }
  % d
}

notesMusic = \notesMel

\akkordZitherTab \notesMusic
