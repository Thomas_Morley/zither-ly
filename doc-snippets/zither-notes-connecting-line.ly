\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a6")

\paper {
  oddHeaderMarkup = ##f
  left-margin = 1 \cm
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-b'-tuning
  }
}

notesConnectingMel = \relative {
  g'1 fis2 g4 a8  g16
  \override Glissando.before-line-breaking =
    #(lambda (grob) (ly:grob-set-property! grob 'stencil #f))
  g
  <g d>1 <fis d>2 <g d>4 a8 <d, g>16 g
}

notesConnectingMusic = \notesConnectingMel

\akkordZitherTab \notesConnectingMusic
