\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a5")

\header {
  zither-dedication = "Dedication"
  zither-title = "Title"
  zither-subtitle = "Subtitle"
  zither-subsubtitle = "Subsubtitle"
  zither-instrument = "Instrument"
  zither-tempo-indication = "Tempo indication"
  zither-annotation = "Annotation"
  zither-composer = "Composer"
  zither-poet = "Poet"
  zither-arranger = "Arranger"
  zither-meter = "Meter"
  zither-piece = "Piece"
  zither-opus = "Opus"
  zither-snippet-doc = "Snippet-documentation"
  zither-image = "Image"
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-c''-tuning
  }
}

\paper {
  markup-system-spacing.padding = 10
}

mus = { g'1 }

\akkordZitherTab \mus
