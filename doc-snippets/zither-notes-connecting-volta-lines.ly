\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a6")

\paper {
  oddHeaderMarkup = ##f
  left-margin = 1 \cm
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-b'-tuning
  }
}

voltaConnectingMel = \relative {
  \repeat volta 2 {
    %% As for common LilyPond, insert a manual repeat bar, if you want it at
    %% start of the piece.
    \bar ".|:"
    d'4 e f e
  }
  \repeat volta 2 {
    d4 e f g
  }
  \alternative {
    { a4~ a a2 }
    { a4 gis a2 }
  }
  \override TextSpanner.details.added-text = \markup \fontsize #-2 "2nd time"
  \repeat volta 2 {
    a4 g f e
  }
  \alternative {
    { d4 f a2 }
    { d,1 }
  }

}

voltaConnectingMusic = \voltaConnectingMel

\akkordZitherTab \voltaConnectingMusic
