\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set! paper-alist
       (cons '("snippet" . (cons (* 200 mm) (* 160 mm))) paper-alist))

#(set-default-paper-size "snippet")

\paper {
  oddHeaderMarkup = ##f
  left-margin = 1 \cm
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-g''-tuning
  }
}

firstVoiceMel = \relative {
  \key g \major
  b'4 c d e
  fis2 fis2
}

secondVoiceMel = \relative {
  g'4 a b c
  d d d d
  \key g \major
}

voicesChrds = \chordmode {
  g2:1 g d1:4 d/fis q q
}

voicesMusic =
<<
    \new ChordNames \voicesChrds
  \new ZitherVoice \firstVoiceMel
  \new ZitherVoice \secondVoiceMel
>>

\akkordZitherTab \voicesMusic
