\version "2.23.9"

%% header for both, traditional and zither notation

\header {
  title = "Hänschen klein ging allein"
  composer = "18. Jh."
  poet = "Franz Wiedemann (1821–1882)"

  zither-title = \title
  zither-composer = "M: 18. Jh."
  zither-poet = "T: Franz Wiedemann (1821–1882)"
  zither-meter = "2/4-Takt"

  texidoc = "foo, bar, buzz"
}

%% Definitions of melody, chords, lyrics, verses

hansMel = \relative {
  \time 2/4
  \key g \major

  \repeat volta 2 {
    \bar ".|:"
    d''8 b b4
    c8 a a4
  }
  \alternative {
    {
      g8 a b c
      d d d4
    }
    {
      g,8 b d d
      b2
    }
  }
  \tag #'trad \break
  a8 a a a
  a b c4
  b8 b b b
  b c d4

  \tag #'trad \break

  d8 b b4
  c8 a a4
  g8 b d d
  g,2
  \bar "|."
}

hansChrds = \chordmode {
  \repeat volta 2 {
    g2 d
  }
  \alternative {
    { g d }
    { g4 d g2 }
  }
  d2 d
  g g4 d

  g2 d
  g4 d g2
}

hansZitherChrds = \chordmode {
  \repeat volta 2 {
    g,4:1 g d4:1 d'/fis
  }
  \alternative {
    { g,:1 g d:1 d'/fis }
    { g4/+g d'/fis/+d g2/+g }
  }
  d4:1 d'/fis
  d4:1 d'/fis
  g,4:1 g

  g4/+g d'/fis/+d
  g,4:1 g d4:1 d'/fis
  g4/+g d'/fis/+d
  g2/+g
}

hansLyr = \lyricmode {
  Häns -- chen klein
  ging al -- lein
  in die wei -- te Welt hin -- ein.
  _ _ _ _
  _
  A -- ber Mut -- ter wei -- net sehr,
  hat ja nun kein Häns -- chen mehr!
  "\"Wünsch" dir "Glück!\""
  sagt ihr Blick,
  "\"kehr'" nur bald zu -- "rück!\""
}

hansLyrAlt = \lyricmode {
  Stock und Hut
  steht ihm gut,
  _ _ _ _
  _ _ _
  ist gar wohl -- ge -- mut.
}

hansVerseI =
\markup
  \column
  \string-lines
    #"1. Hänschen klein
    Ging allein
    In die weite Welt hinein.
    Stock und Hut
    Steht ihm gut,
    Ist gar wohlgemut.
    Aber Mutter weinet sehr,
    Hat ja nun kein Hänschen mehr!
    \"Wünsch dir Glück!\"
    Sagt ihr Blick,
    \"Kehr' nur bald zurück!\""

hansVerseII =
\markup
  \column
  \string-lines
    #"2. Sieben Jahr
    Trüb und klar
    Hänschen in der Fremde war.
    Da besinnt
    Sich das Kind,
    Eilt nach Haus geschwind.
    Doch nun ist's kein Hänschen mehr.
    Nein, ein großer Hans ist er.
    Braun gebrannt
    Stirn und Hand.
    Wird er wohl erkannt?"

hansVerseIII =
\markup
  \column
  \string-lines
    #"3. Eins, zwei, drei
    Geh'n vorbei,
    Wissen nicht, wer das wohl sei.
    Schwester spricht:
    \"Welch Gesicht?\"
    Kennt den Bruder nicht.
    Kommt daher die Mutter sein,
    Schaut ihm kaum ins Aug hinein,
    Ruft sie schon:
    \"Hans, mein Sohn!
    Grüß dich Gott, mein Sohn!\""

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% traditional output
%%%%%%%%%%%%%%%%%%%%%%%%%%%

\book {
  \paper { tagline = ##f }
  \score {
    <<
      \new ChordNames \hansChrds
      \new Staff \hansMel
      \new Staff \hansZitherChrds
      \new Lyrics \lyricsto "" \hansLyr
      \new Lyrics \lyricsto "" \hansLyrAlt
    >>
    \layout {}
  }
  \markup
    \fill-line {
      \hansVerseII
      \hansVerseIII
    }
}
