\version "2.23.9"

\include "../zither-chord.ly"
\include "../zither-tab-init.ly"

#(set-default-paper-size "a7")

\paper {
  oddHeaderMarkup = ##f
  left-margin = 1 \cm
}

\layout {
  \context {
    \ZitherStaff
    stringTunings = #zither-c'-a'-tuning
    \revert Rest.stencil
  }
}

restsMel = \relative {
  d'4 r f8 r a8. r16
}

restsMusic = \restsMel

\akkordZitherTab \restsMusic
