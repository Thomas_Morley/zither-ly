\version "2.23.9"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stem/TabNoteHead
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Stem_notes_engraver =
#(lambda (ctx)
"In an event chord only the note belonging to the melody gets a printed stem.
This engraver sets @code{details.note-markup-stem} to false for other notes of
the same event-chord.
The stencil printing procedure for the @code{TabNoteHead}, i.e.
@code{zither::tab-note-head} reads and reacts on this subproperty."
  (let ((tab-nhds '()))
    (make-engraver
      (acknowledgers
        ((tab-note-head-interface engraver grob source-engraver)
          (set! tab-nhds (cons grob tab-nhds))))
      ((stop-translation-timestep engraver)
       (if (pair? tab-nhds)
           (let ((sorted-tab-nhds ;; in descending order of staff-position
                   (sort
                     tab-nhds
                     (lambda (nhd1 nhd2)
                      (>
                       (ly:grob-property nhd1 'staff-position)
                       (ly:grob-property nhd1 'staff-position))))))
             (if (> (length sorted-tab-nhds) 1)
                 (map
                   (lambda (tnhd)
                     (ly:grob-set-nested-property!
                       tnhd
                       '(details note-markup-stem) #f))
                   (drop-right sorted-tab-nhds 1)))))
       (set! tab-nhds '()))
      ((finalize engraver)
       (set! tab-nhds '())))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Default BarLines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Applies `glissando::draw-zither-tab-glissando', if measurePosition #<Mom 0>
%% Mimics default BarLines, i.e. "|"
Merge_bar_line_glissando_engraver =
#(lambda (context)
  (let ((staff-space #f))
    (make-engraver
      (acknowledgers
        ((staff-symbol-interface engraver grob source-engraver)
         (set! staff-space
               (ly:grob-property grob 'staff-space))))
      (end-acknowledgers
        ((glissando-interface engraver grob source-engraver)
         (if (zero?
               (ly:moment-main (ly:context-property context 'measurePosition)))
             (ly:grob-set-property! grob 'stencil
               (glissando::draw-zither-tab-glissando staff-space)))))
      ((finalize trans)
        (set! staff-space #f)))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Repeat BarLines
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Repeat bar lines are faked as markup, abusing grob StrokeFinger
%% For default-BarLines see above.

Fake_repeat_bars_engraver =
#(lambda (context)
"This engraver prints the locally specified start and end repeat bar lines, by
setting the text property of the here created @code{StrokeFinger} grobs."
  (let* ((start-repeat-bars (list ".|:" "[|:" ".|:-||" ":..:"))
         (end-repeat-bars (list ":|." ":|]" ":..:"))
         (bar-glyph #f)
         (stroke-finger-grob #f)
         (previous-nhd #f)
         (tab-nhds '()))

    (make-engraver
      (acknowledgers
        ((bar-line-interface engraver grob source-engraver)
          (let* ((glyph (ly:grob-property grob 'glyph)))
            (if (and (member glyph (append start-repeat-bars end-repeat-bars)))
                (let ((stroke-finger
                        (ly:engraver-make-grob engraver 'StrokeFinger grob))
                      (used-glyph
                        (car (string-split glyph #\-))))
                  (ly:grob-set-property!
                    stroke-finger
                    'text
                    #{ \markup \vcenter \bar-line #used-glyph #})
                  (set! stroke-finger-grob stroke-finger)
                  (set! bar-glyph used-glyph)))))
        ((tab-note-head-interface engraver grob source-engraver)
          (set! tab-nhds (cons grob tab-nhds))))
        ((stop-translation-timestep engraver)
          (if (pair? tab-nhds)
              (let* ((sorted-tab-nhds ;; in descending order of staff-position
                       (sort
                         tab-nhds
                         (lambda (nhd1 nhd2)
                          (>
                           (ly:grob-property nhd1 'staff-position)
                           (ly:grob-property nhd2 'staff-position)))))
                     (parent-nhd (car sorted-tab-nhds)))
                (if (and (ly:grob? parent-nhd) (ly:grob? stroke-finger-grob))
                    (cond
                      ((member bar-glyph start-repeat-bars)
                        (ly:grob-set-property!
                          stroke-finger-grob 'X-offset -5)
                        (ly:grob-set-parent! stroke-finger-grob X parent-nhd)
                        (ly:grob-set-parent! stroke-finger-grob Y parent-nhd))
                      ((and (ly:grob? previous-nhd)
                            (member bar-glyph end-repeat-bars))
                        (ly:grob-set-property!
                          stroke-finger-grob 'X-offset 1.0)
                        (ly:grob-set-parent!
                          stroke-finger-grob
                          X
                          previous-nhd)
                        (ly:grob-set-parent!
                          stroke-finger-grob
                          Y
                          previous-nhd))
                      (else #f)))
                (set! bar-glyph #f)
                (set! stroke-finger-grob #f)
                (set! previous-nhd parent-nhd)
                (set! tab-nhds '()))))
        ((finalize engraver)
          (set! previous-nhd #f)))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stacked Chords
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define (structure-list l1 l2 rl)
"Lists @var{l1} and @var{l2} are supposed to be lists of moment-grob-pairs,
@var{rl} an empty lst.
Returns a new list of sublist containing grobs happening at the same moment or
later then the relevant entry of @var{l1}.
  Example:
    l1 is:
    ((#<Mom 0> . #<Grob TabNoteHead >)
     (#<Mom 1> . #<Grob TabNoteHead >)
     (#<Mom 15/8> . #<Grob TabNoteHead >)
     (#<Mom 2> . #<Grob TabNoteHead >))

    l2 is:
    ((#<Mom 0> . #<Grob ChordName >)
     (#<Mom 1/2> . #<Grob ChordName >)
     (#<Mom 1> . #<Grob ChordName >)
     (#<Mom 5/4> . #<Grob ChordName >)
     (#<Mom 3/2> . #<Grob ChordName >)
     (#<Mom 7/4> . #<Grob ChordName >)
     (#<Mom 2> . #<Grob ChordName >)
     (#<Mom 5/2> . #<Grob ChordName >))

    returned is:
    ((#<Grob ChordName > #<Grob ChordName >)
     (#<Grob ChordName >
      #<Grob ChordName >
      #<Grob ChordName >
      #<Grob ChordName >)
     (#<Grob ChordName > #<Grob ChordName >))"
;; NB/TODO Elements of l2 before the earliest element of l1 are dropped, ok?
  (if (null? l1)
      (remove null? rl)
      (structure-list
        (drop-right l1 1)
        (remove
          (lambda (x)
            (or (equal? (car (last l1)) (car x))
                (ly:moment<? (car (last l1)) (car x))))
          l2)
        (cons
          ;; keep only the cdr, not the entire pair
          (map
            (lambda (pair) (cdr pair))
            (filter
              (lambda (x)
                (or (equal? (car (last l1)) (car x))
                    (ly:moment<? (car (last l1)) (car x))))
              l2))
          rl))))

Stacked_zither_chords_engraver =
#(lambda (ctx)
"In Unterlegnoten for Akkordzither it is common to print sequential chords
during a long note not distributed along the connecting line, but right after
the initial chord of said long note.
This engraver provides this functionality."
  (let ((tab-note-heads '())
        (chords '()))
    (make-engraver
      (acknowledgers
        ((tab-note-head-interface engraver grob source-engraver)
          (set! tab-note-heads
                (cons
                  (cons (ly:context-current-moment ctx) grob)
                  tab-note-heads)))
        ((chord-name-interface engraver grob source-engraver)
          (set! chords
                (cons
                  (cons (ly:context-current-moment ctx) grob)
                  chords))))
      ((finalize engraver)
        (let ((nhds (reverse tab-note-heads))
              (chrds (reverse chords)))
            (for-each
              (lambda (chrds)
                (let ((texts
                        (map
                          (lambda (chrd) (ly:grob-property chrd 'text))
                          chrds)))
                (ly:grob-set-property!
                  (car chrds)
                  'text
                  #{
                     \markup
                       \halign #CENTER
                       \vcenter
                       \override #'(word-space . 0.6)
                       \line { $texts }
                  #})
                (for-each
                  (lambda (c) (ly:grob-suicide! c))
                  (cdr chrds))))
              (structure-list nhds chrds '())))
        ;; housekeeping:
        (set! tab-note-heads '())
        (set! chords '())))))
