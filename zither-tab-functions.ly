\version "2.23.9"


%% In \paper srfi-1 is not present, thus inserting this here.
%% The definition-body is taken from the module srfi-1, which is present in
%% every .ly-file
#(define iota iota)
#(define pretty-print pretty-print)
#(define last last)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TextSpanner
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Used to indicate when to jump to another place of the tab

#(define (line-spanner::add-text grob)
"Adds text (taken from @code{details.added-text}) to a spanner-line.
With the here used subproperty @code{details.translate-text} expecting a pair,
the text may be moved relative to the line. With '(0 . 0) the text sits on the
line (without gap).

As opposed to non-horizontal ones (like @code{Glissando}, horizontal line
spanners (like @code{TextSpanner}) are sort of special cases.
To get reasonable @code{'Y} from @code{bound-details} we always use
@code{ly:line-spanner::calc-left-bound-info} and
@code{ly:line-spanner::calc-right-bound-info} and not their harizontal version.
Though, for version newer then 2.23.x the stencil is still not accurat.
Thus we need to recreate the stencil."
  (if (not (ly:stencil? (ly:line-spanner::print grob)))
      #f
      (let* ((sys (ly:grob-system grob))
             (layout (ly:grob-layout grob))
             (line-thickness (ly:output-def-lookup layout 'line-thickness))
             (blot-diameter (ly:output-def-lookup layout 'blot-diameter))
             (details (ly:grob-property grob 'details))
             (translate-text
               (assoc-get 'translate-text details '(1 . 1)))
             (added-text
               (assoc-get 'added-text details ""))
             (grob-thickness (ly:grob-property grob 'thickness 1))
             (thick (* grob-thickness line-thickness))
             (left-bound-info
               (ly:line-spanner::calc-left-bound-info grob))
             (left-bound (ly:spanner-bound grob LEFT))
             (left-bound-Y-ext
               (ly:grob-extent left-bound left-bound Y))
             ;; TODO
             ;; y-left from left-bound
             ;; or
             ;; y-left via left-bound-info
             ;; ??
             (y-left (cdr left-bound-Y-ext))
             ;;(y-left (assoc-get 'Y left-bound-info))
             (x-left (assoc-get 'X left-bound-info))
             (left-padding (assoc-get 'padding left-bound-info))
             (right-bound-info
               (ly:line-spanner::calc-right-bound-info grob))
             (right-bound (ly:spanner-bound grob RIGHT))
             (right-bound-Y-ext
               (ly:grob-extent right-bound right-bound Y))
             ;; TODO
             ;; y-right from right-bound
             ;; or
             ;; y-right via right-bound-info
             ;; ??
             (y-right (cdr right-bound-Y-ext))
             ;; (y-right (assoc-get 'Y right-bound-info))
             (x-right (assoc-get 'X right-bound-info))
             (slant (sign (- y-right y-left)))
             (alpha
               (ly:angle
                 (- x-right x-left)
                 (- y-right y-left)))
             (spanner-center-X
               (interval-center (cons 0 (- x-right x-left))))
             (text-stencil
               (if (zero? slant)
                   (grob-interpret-markup grob added-text)
                   (ly:stencil-scale
                     (grob-interpret-markup grob added-text)
                     slant slant)))
             (rotated-text-stil
               (ly:stencil-rotate text-stencil alpha 0 0))
             (text-center-X
               (interval-center (ly:stencil-extent rotated-text-stil X)))
             (translated-text-stencil
               (ly:stencil-translate
                 rotated-text-stil
                 (cons
                   (- (- spanner-center-X text-center-X)
                      (car translate-text))
                   (+ (/ (+ y-right y-left) 2)
                      (* (if (zero? slant) 1 slant)
                         (cdr translate-text)))))))
        (ly:stencil-add
          (ly:line-interface::line grob 0 y-left (- x-right x-left) y-right)
          translated-text-stencil))))

autoVoltaFollower =
#(define-music-function (music)(ly:music?)
"Add a line connecting last element of repeat body and first element of second
volta, by inserting start/stop of a TextSpanner."
;; For version 2.23.9
;; NB only older syntax like
;;   \repeat volta 2 { ... }
;;   and
;;   \repeat volta 2 { ... } \alternative { { ... } { ... } }
;;   supported, not the pretty new
;;   \repeat volta 2 { ... \alternative { { ... } { ... } } }
;;
;; VoltaRepeatedMusic without alternatives looks like:
;;  (make-music
;;    'VoltaRepeatedMusic
;;    'elements '()
;;    'repeat-count 2
;;    'element
;;    (make-music
;;      'SequentialMusic
;;      'elements <list of note-events>))
;;
;; ;; VoltaRepeatedMusic with alternatives looks like, e.g.:
;;  (make-music
;;    'VoltaRepeatedMusic
;;    'elements
;;    (list (make-music
;;            'VoltaSpeccedMusic
;;            'volta-numbers (list 1)
;;            'element
;;            (make-music
;;              'SequentialMusic
;;              'elements <list of note-events>))
;;          (make-music
;;            'VoltaSpeccedMusic
;;            'volta-numbers (list 2)
;;            'element
;;            (make-music
;;              'SequentialMusic
;;              'elements <list of note-events>)))
;;    'repeat-count 2
;;    'element
;;    (make-music
;;      'SequentialMusic
;;      'elements <list of note-events>)))
;;
;;  i.e. the content of the voltas is stored in 'elements of VoltaRepeatedMusic
;;       the "body" is stored in 'element of VoltaRepeatedMusic
  (let* ((volta-repeated-music (extract-named-music music 'VoltaRepeatedMusic)))
    (for-each
      (lambda (v-r-m)
        (let* ((volta-repeated-alternatives
                 (ly:music-property v-r-m 'elements)))
          ;; If no alternatives are present return unchanged.
          ;; Otherwise insert start/stop of a TextSpanner connecting last
          ;; element of Body and first element of second volta.
          ;; More than 2 voltas are not supported.
          (if (pair? volta-repeated-alternatives)
              (let* (;; Look at the "body": \repeat volta 2 { BODY }
                     ;;
                     ;; BODY is the 'element of VoltaRepeatedMusic,
                     ;; usually sequential music
                     (volta-body
                       (ly:music-property v-r-m 'element))
                     ;; Get the elements-list of BODY
                     (volta-body-elts
                       (ly:music-property volta-body 'elements))
                     ;; Keep only note-events and event-chords of BODY
                     (volta-body-notes-and-evt-chrds
                       (filter
                         (lambda (elt)
                           (or (eq? (ly:music-property elt 'name) 'NoteEvent)
                               (eq? (ly:music-property elt 'name) 'EventChord)))
                         volta-body-elts))
                     ;; Get the last one.
                     ;; If voltas are present this element serves as start point
                     ;; of the following line
                     (body-last-note-or-evt-chrd
                       (last volta-body-notes-and-evt-chrds))
                     ;; Look at last alternative
                     (last-alternative (last volta-repeated-alternatives))
                     (last-alternative-elts
                       (ly:music-property
                         (ly:music-property last-alternative 'element)
                         'elements))
                     (last-alternative-notes-and-evt-chrds
                       (filter
                         (lambda (elt)
                           (or (eq? (ly:music-property elt 'name)
                                    'NoteEvent)
                               (eq? (ly:music-property elt 'name)
                                    'EventChord)))
                         last-alternative-elts))
                     (last-volta-first-note-or-evt-chord
                       (car last-alternative-notes-and-evt-chrds)))

                ;; insert start of TextSpanner
                (ly:music-set-property!
                  body-last-note-or-evt-chrd
                  'articulations
                  (cons
                    (make-music
                      'TextSpanEvent
                      'tweaks
                      (list (cons 'stencil line-spanner::add-text))
                      'span-direction -1)
                    (ly:music-property
                      body-last-note-or-evt-chrd
                      'articulations)))

                ;; insert end of TextSpanner
                (ly:music-set-property!
                  last-volta-first-note-or-evt-chord
                  'articulations
                  (cons
                    (make-music
                      'TextSpanEvent
                      'span-direction 1)
                    (ly:music-property
                      last-volta-first-note-or-evt-chord
                      'articulations)))
                v-r-m)
              v-r-m)))
      volta-repeated-music)
    music))

%% For manual usage:

voltaLine = \startTextSpan
endVoltaLine = \stopTextSpan

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TabNoteHead
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% overriding the default-stencil with note-markups

#(define zither::tab-note-head
  (lambda (grob)
    (let* ((cause (ly:grob-property grob 'cause))
           (dur (ly:prob-property cause 'duration))
           (dur-log (ly:duration-log dur))
           (dots (ly:duration-dot-count dur))
           (compress-factor (ly:duration-factor dur))
           (display-cautionary
             (ly:grob-property grob 'display-cautionary #f))
           (details (ly:grob-property grob 'details))
           (note-markup-stem
             (assoc-get 'note-markup-stem details #t))
           (rotated-note-head-stil
             (ly:stencil-rotate
               (grob-interpret-markup grob
                 #{
                   \markup {
                     %% TODO ugh, hardcoded values, calculate them!
                     \raise #(if (> dur-log 0) -0.55 -0.75)
                     \with-dimensions #'(0 . 0) #'(0 . 0)
                     $(let ((zither-note-markup
                             (if note-markup-stem
                                 (make-note-markup
                                   (if (ly:version? > '(2 22 0))
                                       (ly:make-duration
                                          dur-log
                                          dots
                                          (car compress-factor)
                                          (cdr compress-factor))
                                       (ly:duration->string dur))
                                   1)
                                 (make-musicglyph-markup
                                   (format #f "noteheads.s~a"
                                     (if (>= dur-log 2)
                                         2
                                         dur-log))))))
                     (if display-cautionary
                         (make-override-markup
                           '((padding . 0.45)
                             (line-thickness . 0.2)
                             (width . 0.3)
                             (size . 1.2))
                           (make-parenthesize-markup zither-note-markup))
                         zither-note-markup))
                   }
                 #})
               90 0 0)))
   (ly:make-stencil
       (ly:stencil-expr rotated-note-head-stil)
     ;;  TODO urgh, hard-coded
     '(-2.5 . 0)
     '(0 . 0)))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Glissando
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Used to indicate the sequence of the music

#(define ((glissando::draw-zither-tab-glissando staff-space) grob)
"Adds a line to Glissando.stencil, but not to the last part if broken."
  (let* ((default-stil (glissando::draw-tab-glissando grob))
         (default-stil-x-ext (ly:stencil-extent default-stil X))
         (default-stil-y-ext (ly:stencil-extent default-stil Y))
         (orig (ly:grob-original grob))
         (siblings (if (ly:grob? orig)
                       (ly:spanner-broken-into orig)
                       '()))
         (bar-adding
           (if (or (null? siblings) (not (equal? (last siblings) grob)))
               (ly:stencil-translate
                 (centered-stencil
                   (grob-interpret-markup grob
                     (markup
                       #:override '(thickness . 1.6)
                       #:draw-line (cons (* staff-space 1/3) 0))))
                 (cons
                   (/ (interval-length default-stil-x-ext) 2)
                   (/ (+ (cdr default-stil-y-ext) (car default-stil-y-ext)) 2)))
               empty-stencil)))
    (ly:stencil-add
       default-stil
       bar-adding)))

allGlissando =
#(define-music-function (music)(ly:music?)
"Adds a glissando-event to all note-events, but not to the last one of a score
and not to the last note-event of any volta-repeats, except the last."
  (let* (
         ;; TODO cleanup extracting things
         (evt-chords (extract-named-music music 'EventChord))
         (note-evts (extract-named-music music 'NoteEvent))
         (volta-repeated-music (extract-named-music music 'VoltaRepeatedMusic))
         (notes-and-evt-chords
           (extract-music
             music
             (lambda (m)
               (or (music-is-of-type? m 'event-chord)
                   (music-is-of-type? m 'note-event)))))
         (drop-last-if-event-chord
           (lambda (lst)
             "The name says it all"
             (if (and (pair? lst) (music-is-of-type? (last lst) 'event-chord))
                 (drop-right lst 1)
                 lst)))
         ;; TODO
         ;; fiddling with nested sublists is tedious, better do recursing
         ;; or maybe do an engraver?
         (v-r-m-elts-raw
           (if (pair? volta-repeated-music)
               (map
                 (lambda (x) (ly:music-property x 'elements))
                   volta-repeated-music)
               '()))
         (v-r-m-elts
           (if (ly:version? > '(2 23 5))
               (map
                 (lambda (x)
                   (append-map
                     (lambda (m)
                       (extract-named-music m 'SequentialMusic))
                     x))
                  v-r-m-elts-raw)
               v-r-m-elts-raw))
        (volta-note-evts-list
          (if (pair? v-r-m-elts)
              (map
                (lambda (l)
                  (map
                    (lambda (y)
                      (filter
                        (lambda (m)
                          (or (music-is-of-type? m 'event-chord)
                              (music-is-of-type? m 'note-event)))
                        (ly:music-property y 'elements)))
                    (if (> (length l) 0)
                        (drop-right l 1)
                        l)))
                 v-r-m-elts)
              '()))
        (targets
          (flatten-list
            (map
              (lambda (x)
                (map
                  (lambda (l)
                    (if (> (length l) 1)
                        (take-right l 1)
                        l))
                  x))
              volta-note-evts-list))))

    (if (pair? targets)
        (for-each
          (lambda (z)
            (ly:music-set-property! z 'tweaks
              (acons 'id 'no-gliss
                (ly:music-property z 'tweaks))))
          targets))

    ;; initiate glissando for event-chords
    (for-each
      (lambda (elt)
        (if (and (music-is-of-type? elt 'event-chord)
                 (not (member '(id . no-gliss) (ly:music-property elt 'tweaks))))
            (ly:music-set-property! elt 'elements
              (cons
                (make-music 'GlissandoEvent)
                (ly:music-property elt 'elements)))))
      (drop-last-if-event-chord notes-and-evt-chords))

    ;; initiate glissando for single note-events
    ;; TODO don't do it for note-events of event-chords, event-chords are done
    (for-each
      (lambda (n-evt)
        (if (not (member '(id . no-gliss) (ly:music-property n-evt 'tweaks)))
            (ly:music-set-property! n-evt 'articulations
              (cons
                (make-music 'GlissandoEvent)
                (ly:music-property n-evt 'articulations)))))
      (drop-right note-evts 1))

    music))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% StaffSymbol with thick line at right side
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#(define (thick-line factor staff-space)
  (lambda (grob)
    (let* ((default-stil (ly:staff-symbol::print grob))
           (thick-dark-grey-line
             (stencil-with-color
                (make-filled-box-stencil
                  (ly:stencil-extent default-stil X)
                  '(-0.2 . 0.2))
                '(0.5 0.5 0.5))))
     (ly:stencil-add
       default-stil
       (ly:stencil-translate-axis
         thick-dark-grey-line
         (* staff-space factor)
         Y)))))

#(define thick-staff-symbol-line
  (lambda (ctx)
    (if (ly:context-property ctx 'emphaziseString)
        (let* ((zither-tuning-pitches
                 (reverse (current-string-tuning $defaultlayout 'ZitherStaff)))
               (strngs (length zither-tuning-pitches))
               (staff-symbol-props
                 (ly:context-grob-definition ctx 'StaffSymbol))
               (staff-space
                 (assoc-get 'staff-space staff-symbol-props))
               (emphazised-pitch (ly:context-property ctx 'emphazisedPitch))
               (emphazised-pitch-pos
                 (list-index
                   (lambda (p) (equal? p emphazised-pitch))
                   zither-tuning-pitches)))
          (ly:context-pushpop-property ctx 'StaffSymbol 'stencil
            (lambda (g)
              ((thick-line
                 (- (/ (1- strngs) 2) emphazised-pitch-pos)
                 staff-space)
                 g)))))))

staffSymbolWithOneThickLine =
#(make-apply-context thick-staff-symbol-line)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ChordNames
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% TODO
%% Current implementation sets TabNoteHead as parent for ChordName
%% ChordName.stencil is a centered stencil
%% This means TabNoteHead and ChordName are printed upon each other, with the
%% need to do manual corrections for every ChordName.
%% Better method?

setTabNoteHeadChordNameParent =
\override ChordName.before-line-breaking =
  #(lambda (grob)
    ;; We go for the first found TabNoteHead.
    ;; NB this is not really safe ...
    (let* ((pap-col (ly:grob-parent grob X))
           (pap-col-elts
             (ly:grob-array->list (ly:grob-object pap-col 'elements)))
           (tab-heads
             (filter
               (lambda (g)
                 (grob::has-interface g 'tab-note-head-interface))
               pap-col-elts)))
      (if (pair? tab-heads)
          (ly:grob-set-parent! grob Y (car tab-heads)))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Tie
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Ties are not printed. The tied note head is parenthesized.

#(define (tie::handle-zither-tab-note-head grob)
"Sets @code{display-cautionary} true for the tied @code{TabNoteHead}.
The procedure building the stencil for the tied @code{TabNoteHead}, will then
parenthesize said stencil."
  (let* ((tied-note-head (ly:spanner-bound grob RIGHT)))
    (ly:grob-set-property! tied-note-head 'display-cautionary #t)))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Stem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Used to connect all note heads of an event-chord

#(define event-chord-connecting-line
  (lambda (grob)
    (let* ((note-heads-array (ly:grob-object grob 'note-heads #f)))
      (if note-heads-array
          (let* ((note-heads-list
                   (if note-heads-array
                       (ly:grob-array->list note-heads-array)
                       '()))
                 (staff-pos-list
                   (sort
                     (map
                       (lambda (nhd)
                         (ly:grob-property nhd 'staff-position))
                       note-heads-list)
                     <))
                 (duration-log (ly:grob-property grob 'duration-log))
                 (start-end-x
                   (if (> duration-log 0)
                       0
                       1.25))
                 (staff-space (ly:staff-symbol-staff-space grob)))

            (ly:line-interface::line
              grob
              ;; startx
              start-end-x
              ;; starty
              (* (car staff-pos-list) (/ staff-space 2))
              ;; endx
              start-end-x
              ;; endy
              (* (last staff-pos-list) (/ staff-space 2))))
          (ly:stem::print grob)))))

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Final outputting function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%{
#(ly:message
"Limitations/TODOs\n
- \\headers are not possible in \\score or \\bookpart due to with-header-markup (LSR 467)
- cutOffLines
- DOCME TextSpanner.details.translate-text  (a pair)
\n
")
%}

akkordZitherTab =
#(define-scheme-function (tab-y music)((number? 0) ly:music?)
  "Takes @var{music} score, returns tabulatur for Akkordzither as one markup.
@code{ZitherStaff} contains always not more than one page.
The applied @code{\\zither-header-tab} returns some text, taken from
@code{\\header} and a @code{ZitherStaff}.
The optional @var{tab-y} moves the @code{ZitherStaff} relative to the text."
#{
  \markup { \override #`(tab-y-offset . ,tab-y) \zither-header-tab $music }
#})
