\version "2.23.9"

#(define-markup-command (parallel-lines layout props dest amount)(pair? index?)
"DOCME"
  (let* ((line
           (interpret-markup layout props (make-draw-dashed-line-markup dest))))
    (apply
      ly:stencil-add
      (map
        (lambda (i stil) (ly:stencil-translate stil (cons -2 i)))
        (iota amount 0 -4)
        (make-list amount line)))))

cutOffLines =
  \markup {
      \with-dimensions #'(0 . 0) #'(0 . 0)
      \translate #'(0 . -20)
      \overlay {
        \parallel-lines #'(80 . 52) #8
      }
  }

akkordZitherBookTitle =
\markup
  \with-header
  \line {
    \override #'(baseline-skip . 3.5)
    \column {
      \line {
          \column {
            \fromproperty #'header:zither-dedication
            \huge \larger \larger \bold\fromproperty #'header:zither-title
            \large \bold \fromproperty #'header:zither-subtitle
            \smaller \bold\fromproperty #'header:zither-subsubtitle
            \fromproperty #'header:zither-instrument
            \override #'(word-space . 8)
            \line {
              \fromproperty #'header:zither-tempo-indication
              \fromproperty #'header:zither-annotation
            }
            \column {
              \fromproperty #'header:zither-composer
              \fromproperty #'header:zither-poet
              \fromproperty #'header:zither-arranger
              \fromproperty #'header:zither-meter
              \override #'(word-space . 8)
              \line {
                \fromproperty #'header:zither-piece
                \fromproperty #'header:zither-opus
              }
            }
          }
      }
      \fromproperty #'header:zither-snippet-doc
      \fromproperty #'header:zither-image
    }
  }

%% NOTA BENE
%%
%% Currently we use "a4landscape", which should be rotated to `portrait' later
%%     With landscape it looks like (graphic annotated with defaults):
%%
%%                    top-margin
%%                 ----------------
%%                 |              |
%%     left-margin |              | right-margin
%%                 |              |
%%                 ----------------
%%                   bottom-margin
%%
%%     If rotated it becomes:
%%
%%                   left-margin
%%                    --------
%%                    |       |
%%                    |       |
%%     bottom-margin  |       | top-margin
%%                    |       |
%%                    |       |
%%                    |       |
%%                    ---------
%%                   right-margin
%%

\paper {
   zitherBookTitleMarkup = \akkordZitherBookTitle
   oddHeaderMarkup =
     \markup \fill-line { \null \rotate #-90 \cutOffLines }
   evenHeaderMarkup = ##f
   tagline = ##f
}
