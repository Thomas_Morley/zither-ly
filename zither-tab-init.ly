\version "2.23.9"

\include "zither-paper-defaults.ly"
\include "zither-tab-markup-commands.ly"
\include "zither-tab-titling.ly"
\include "zither-tab-functions.ly"
%\include "zither-chord.ly"
\include "zither-scheme-engraver.ly"
\include "zither-tab-layout.ly"
